package com.jdbc.gui.example;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class JDBCGUIExample extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -682773581737329678L;
	private JTextField customerField;

	private JLabel customeLabel;
	private JButton searchBtn;
	private JButton clearBtn;
	private Connection conn;
	private JTable tbl;
	private DefaultTableModel dm;

	public JDBCGUIExample() {
		super("Customer-Order");
		this.setDBConnection();
		Container container = this.getContentPane();

		// Construct label and text field GUI components
		customeLabel = new JLabel("Customer Name:");
		customeLabel.setBounds(10, 80, 96, 14);
		container.add(customeLabel);
		customerField = new JTextField(15);
		customerField.setBounds(116, 76, 126, 20);
		container.add(customerField);
		searchBtn = new JButton("Get Total");
		searchBtn.setBounds(26, 135, 94, 23);
		container.add(searchBtn);

		//register button click event
		searchBtn.addActionListener(this);

		clearBtn = new JButton("Clear");
		clearBtn.setBounds(136, 135, 94, 23);
		container.add(clearBtn);

		//register button click event
		clearBtn.addActionListener(this);

		dm = new DefaultTableModel(0, 0);
		String header[] = new String[] { "Customer", "Amout Total" };
		dm.setColumnIdentifiers(header);
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		tbl = new JTable();
		tbl.setModel(dm);

		JScrollPane tableContainer = new JScrollPane(tbl);

		panel.add(tableContainer, BorderLayout.CENTER);
		container.add(panel);

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				closeDBConnection();
				System.exit(0);
			}
		});

		setSize(343, 218);
		setVisible(true);
	}

	public void setDBConnection() {
		Properties prop = new Properties();
		InputStream input = null;

		input = ClassLoader.getSystemResourceAsStream("jdbc.properties");
		//load DB connection info
		try {
			prop.load(input);

			String driverClassName = prop.getProperty("jdbc.driverClassName");
			String url = prop.getProperty("jdbc.url");
			String userName = prop.getProperty("jdbc.username");
			String password = prop.getProperty("jdbc.password");

			// load JDBC driver
			Class.forName(driverClassName).newInstance();

			// connect to mysql database
			conn = DriverManager.getConnection(url, userName, password);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		catch (InstantiationException e) {
			e.printStackTrace();
		}
		catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void closeDBConnection() {
		if (conn != null) {
			try {
				conn.close();
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == searchBtn) {
			String sql = "SELECT CUSTOMERS.name, sum(ORDERS.amount) as total FROM CUSTOMERS"
					+ ", ORDERS WHERE ORDERS.CUSTOMER_ID = customers.CUSTOMER_ID AND lower(CUSTOMERS.NAME) like ? group by CUSTOMERS.name";
			try {
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, "%" + customerField.getText().toLowerCase() + "%");
				ResultSet rs = ps.executeQuery();

				if (rs.next()) {
					Vector<Object> data = new Vector<Object>();
					data.add(rs.getString("name"));
					data.add(rs.getDouble("total"));
					dm.addRow(data);
				}
				rs.close();
			}
			catch (SQLException ex) {
			}
		}
		else if (event.getSource() == clearBtn) {
			customerField.setText("");
			dm.removeRow(0);
		}
	}

	public static void main(String[] args) {
		new JDBCGUIExample();
	}

}
